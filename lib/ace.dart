@JS()
library ace;

import 'package:js/js.dart';

// ignore: avoid_web_libraries_in_flutter
import 'dart:html';

import 'package:js/js_util.dart';

external Ace get ace;

@JS('ace')
class Ace {
  external static EditSession createEditSession(dynamic text, String mode);

  external static dynamic require(String module);

  external static Editor edit(DivElement el);
}

@JS()
class Editor {
  external Editor(VirtualRenderer renderer, EditSession session);

  external EditSession get session;

  external set session(EditSession session);

  external EditSession getSession();

  external setSession(EditSession session);

  external String getTheme();

  external setTheme(String theme);

  external setFontSize(int size);

  external setValue(String text, int cursorPos);

  external String getValue();

  external moveCursorTo(int row, int column);

  external onTextInput();
}

@JS()
class EditSession {
  external EditSession(dynamic text, String mode);

  external set doc(Document doc);

  external Document get doc;

  external set document(Document doc);

  external Document get document;

  external String getValue();

  external setValue(String text);

  external int getLength();

  external String getLine(int row);

  external List<String> getLines(int firstRow, int lastRow);

  external String getSelection();

  external set mode(String mode);

  external String get mode;

  external setMode(String mode);

  external String getMode();

  external getAWordRange(int row, int column);
}

@JS()
@anonymous
class VirtualRenderer {
  external factory VirtualRenderer({DivElement container, String theme});

  external String get theme;

  external set theme(String theme);

  external updateCursor();

  external updateFull(bool force);

  external updateText();
}

Document newDocument(String text) {
  final docObject = callMethod(ace, "require", ["ace/document"]).Document;
  return callConstructor(docObject, [text]);
}

@JS()
@anonymous
class Document {
  external Document(String text);

  external String getValue();

  external setValue(String text);

  external int getLength();

  external String getLine(int row);

  external List<String> getAllLines();

  external String getNewLineCharacter();

  external String getNewLineMode();

  external dynamic insert(dynamic position, String text);
}

@JS()
@anonymous
class Selection {
  external Selection(EditSession session);

  external int getCursor();

  external moveCursorDown();

  external moveCursorLeft();

  external moveCursorRight();

  external moveCursorLineStart();

  external moveCursorToPosition(dynamic position);

  external selectAWord();

  external selectWord();

  external selectLeft();

  external selectRight();
}

@JS()
@anonymous
class Anchor {
  external Anchor(Document doc, int row, int column);

  external detach();

  external Document getDocument();

  external dynamic getPosition();

  external onChange();

  external setPosition(int row, int column, bool noClip);
}
