import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_interop/ace.dart';
import 'package:js/js.dart';
import 'dart:ui' as ui;

import 'dart:html';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({this.title = "Flutter Interop JS"});

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DivElement div = DivElement();
  UniqueKey elementId;
  Editor _editor;
  Document _doc;
  EditSession _session;
  VirtualRenderer _renderer;

  @override
  void initState() {
    super.initState();
    elementId = UniqueKey();
    // ignore: undefined_prefixed_name
    ui.platformViewRegistry
        .registerViewFactory(elementId.toString(), (int viewId) {
      _editor = Ace.edit(div);
      _editor.setTheme("ace/theme/monokai");
      _editor.session.mode = "ace/mode/golang";
      _editor.setFontSize(20);
      return div;
    });

    // _doc = newDocument("some code\ntwo lines");
    // _editor.session.doc = _doc;

    // _session = Ace.createEditSession(_doc, "ace/mode/golang");
    // _renderer =
    //     VirtualRenderer(container: div, theme: "ace/theme/monokai");
    // _editor = Editor(renderer: _renderer, session: _session);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Container(
            width: 500,
            height: 500,
            color: Colors.white70,
            child: HtmlElementView(
              key: elementId,
              viewType: elementId.toString(),
            )),
        Center(child: RaisedButton(
          onPressed: () {
            if (mounted) {
              _editor.setValue("some code\ntwo lines", 0);
              // log(_doc.getValue().toString());
            }
          },
        )),
      ]),
    );
  }
}
